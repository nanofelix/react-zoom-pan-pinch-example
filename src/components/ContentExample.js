import React, { Component } from "react";

import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

import './ContentExample.css';

const image = require("../images/floor.jpg")

class ContentExample extends Component {
    render() {
        return (
            <TransformWrapper
                initialScale={0.15}
                minScale={0.1}
                maxScale={3}
                centerOnInit={true}
                alignmentAnimation={{
                    sizeY: 0,
                    sizeX: 0,
                }}
            >
                <TransformComponent>
                    <img alt="test" src={image} />
                </TransformComponent>
            </TransformWrapper>
        );
    }
}

export default ContentExample;


