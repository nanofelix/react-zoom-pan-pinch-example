import ContentExample from './components/ContentExample';

function App() {
  return (
    <ContentExample />
  );
}

export default App;
